#!/usr/bin/env python
# -*- coding: latin-1 -*-
import ipguard_pcol
import sys

#----------------------------------------------------------------------------
if __name__=='__main__':
    port = '/dev/serial0'
    if (len(sys.argv) == 2):
        port = sys.argv[1]

    ipguard_pcol.init(port)
    (m, v, sn) = ipguard_pcol.WhoAreYou()
    print "Model;%04x;Version;%04x;SN;%08x" % (m, v, sn)
    ipguard_pcol.deinit()

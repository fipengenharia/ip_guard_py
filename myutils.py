#!/usr/bin/env python
# -*- coding: latin-1 -*-
import datetime

#----------------------------------------------------------------------------
def DebugBytes (bf):
    result = ''
    for v in bf:
        result =  result + ''.join('%02X'%ord(v)) + ' '
    return result

#----------------------------------------------------------------------------
def Log(msg):
    now = datetime.datetime.now()
    print now.strftime("[%Y-%m-%d %H:%M:%S]"), msg


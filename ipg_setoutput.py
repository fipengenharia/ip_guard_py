#!/usr/bin/env python
# -*- coding: latin-1 -*-
import ipguard_pcol
import sys

#----------------------------------------------------------------------------
if __name__=='__main__':
    port = '/dev/serial0'
    args = len(sys.argv)
    if ((args >= 3) and (args <= 4)):
        data = int(sys.argv[1], base=16)
        mask = int(sys.argv[2], base=16)
        if (args == 4):
            port = sys.argv[3]
    else:
        print "Usage: ipg_seroutput.py relays mask [port]"
        quit()

    ipguard_pcol.init(port)
    res = ipguard_pcol.SetOutputs(data, mask)
    if (res == 0):
        (i,o,kid) = ipguard_pcol.GetStatus()
        print "Outputs;%02x" % (o)
    else:
        print "Res;%d" % (res)

    ipguard_pcol.deinit()

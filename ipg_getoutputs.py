#!/usr/bin/env python
# -*- coding: latin-1 -*-
import ipguard_pcol
import sys

#----------------------------------------------------------------------------
if __name__=='__main__':
    port = '/dev/serial0'
    if (len(sys.argv) == 2):
        port = sys.argv[1]

    ipguard_pcol.init(port)
    (i,o,kid) = ipguard_pcol.GetStatus()
    print "Outputs;%02x" % (o)
    ipguard_pcol.deinit()

# IPGUARD - SCRIPTS EM PYTHON

Conjuntos de scripts em Python para a comunicação com a placa IpGuard via protocolo
de comunicação serial

## Descrição dos arquivos

* ipg_getinpus.py - leitura das entradas digitais
* ipg_getkey.py - Leitura das teclas
* ipg_getoutputs.py - Leitura do estado das saídas
* ipg_getrfid.py - Leitura do RFID
* ipg_getvin.py - Leitura da tensão de alimentação
* ipg_ping - Comando PING ( Informa que está vivo )
* ipg_setoutput.py - Aciona saídas
* ipg_whoareyou - Informacoes do PIC ( modelo, versão )
* proto.py - Protocolo serial, tratamento dos bytes
* ipguard_pcol.py - Parser dos comandos
* myutils - funcoes comuns


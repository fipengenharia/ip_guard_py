#!/usr/bin/env python
# -*- coding: latin-1 -*-
import ipguard_pcol
import sys

#----------------------------------------------------------------------------
if __name__=='__main__':
    port = '/dev/serial0'
    if (len(sys.argv) == 2):
        port = sys.argv[1]

    ipguard_pcol.init(port)
    (i,o,kid) = ipguard_pcol.GetStatus()
    if (i == -1):
        rfid = 'TIMEOUT'
    else:
        rfid = ''
        if ((kid & 0x0f) != 0):
            rfid = ipguard_pcol.GetRFID()
    print "ID;%s" % (rfid)
    ipguard_pcol.deinit()

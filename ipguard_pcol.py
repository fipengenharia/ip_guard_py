#!/usr/bin/env python
# -*- coding: latin-1 -*-
from   struct import pack, unpack
from   myutils import Log
import proto

# Device <-> Host Pcol commands
cteHostCmd_WhoAreYou         = '\x10'
cteHostCmd_Ping              = '\x11'
cteHostCmd_Get_Status        = '\x12'
cteHostCmd_Get_Key           = '\x13'
cteHostCmd_Get_RFID          = '\x14'
cteHostCmd_Get_Vin           = '\x15'
cteHostCmd_Set_Outputs       = '\x16'
#
#
cteHostRet_WhoAreYou         = '\x90'
cteHostRet_Ok                = '\x91'
cteHostRet_Data              = '\x92'
cteHostRet_Error             = '\x93'

#v.3 = Verbose Answer
Verbose_Answer = 0x08

#----------------------------------------------------------------------------
def init (port, to = .1):
    proto.init(port, baud = 38400, tout = to)

#----------------------------------------------------------------------------
def deinit ():
    proto.deinit()

#----------------------------------------------------------------------------
def WhoAreYou (v = 0):
    ret = (-1,0,0)
    b = proto.TxRxHostCmd( cteHostCmd_WhoAreYou )
    if (b):
        if (b[0] == cteHostRet_WhoAreYou):
            (answer, sz, model, version, sn ) = unpack( '=BBHHI', b )
            ret = (model, version, sn)
            if (v & Verbose_Answer):
                Log("Hw Model: 0x%04x"   % model)
                Log("Hw Ver: 0x%04x"   % version)
                Log("SN: %08d%" % sn)
        else:
            ret = (-2,0,0)
    return ret

#----------------------------------------------------------------------------
def Ping ():
    ret = -1
    b = proto.TxRxHostCmd( cteHostCmd_Ping)
    if (b):
        if (b[0] == cteHostRet_Ok):
            ret = 0
        else:
            ret = -2
    return ret

#----------------------------------------------------------------------------
def GetStatus():
    ret = (-1,0,0)
    b = proto.TxRxHostCmd( cteHostCmd_Get_Status,'',3,0)
    if (b):
        if (b[0] == cteHostRet_Data):
            (answer, sz, inputs, outputs, keyid ) = unpack( '=BBBBB', b )
            ret = (inputs, outputs, keyid)
        else:
            ret = (-2,0,0)
    return ret

#----------------------------------------------------------------------------
def GetKey():
    ret = 'TIMEOUT'
    b = proto.TxRxHostCmd( cteHostCmd_Get_Key,'',3,0)
    if (b):
        if (b[0] == cteHostRet_Data):
            ret = b[2:]
        else:
            ret = 'ERROR'
    return ret

#----------------------------------------------------------------------------
def GetRFID():
    ret = 'TIMEOUT'
    b = proto.TxRxHostCmd( cteHostCmd_Get_RFID,'',3,0)
    if (b):
        if (b[0] == cteHostRet_Data):
            ret = b[2:]
        else:
            ret = 'ERROR'
    return ret

#----------------------------------------------------------------------------
def GetVin():
    ret = -1
    b = proto.TxRxHostCmd( cteHostCmd_Get_Vin)
    if (b):
        if (b[0] == cteHostRet_Data):
            (answer, sz, vin ) = unpack( '=BBH', b )
            ret =  vin
        else:
            ret = -2
    return ret

#----------------------------------------------------------------------------
def SetOutputs (state, mask):
    ret = -1
    data = str(chr(state) + chr(mask))
    b = proto.TxRxHostCmd( cteHostCmd_Set_Outputs, data, 3, 0)
    if (b):
        if (b[0] == cteHostRet_Ok):
            ret = 0
        else:
            ret = -2
    return ret

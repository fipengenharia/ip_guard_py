#!/usr/bin/env python
# -- coding: utf-8 --

import time
import os
import sys
import threading

import ipguard_pcol

from flask import Flask
from flask import request
from flask import jsonify

rele=3
delay=10

if os.access("/var/www/html/scripts/rfidrele.cnf", os.R_OK):
    with open("/var/www/html/scripts/rfidrele.cnf") as fp:
        rele=''.join(str(fp.read()).split())

if os.access("/var/www/html/scripts/rfiddelay.cnf", os.R_OK):
    with open("/var/www/html/scripts/rfiddelay.cnf") as fp:
        delay=''.join(str(fp.read()).split())


app = Flask(__name__)

ipg_getrfid =''



port = '/dev/serial0'
ipguard_pcol.init(port)

block =False

def isUnBlock():
  while block:
    time.sleep(0.01)
    pass
  return True

def atualizador():
  print 'Start thread'
  global ipg_getrfid
  global ipg_getinpus
  global ipg_getoutputs
  global ipg_getvin
  global block
  teclado = {"A": "q", "B": "e", "C": "z","P": "c","U": "Up","D": "Down","R": "Right","L": "Left","1": "1","2": "2","3": "3","4": "4","5": "5","6": "6","7": "7","8": "8","9": "9","0": "0"}

 
  while True: 
    time.sleep(0.05)
    if (not block):
      #----------------------------------------------------------------------------ipg_getkey
      inputkeys = str(ipguard_pcol.GetKey())
      k = list(inputkeys)     
      for key in k:        
        os.popen("DISPLAY=:0 xdotool key "+teclado[key]+" &")
        print 'tecla: ( '+key + ') chave: ('+teclado[key]+')'
      time.sleep(0.02)
      (i,o,kid) = ipguard_pcol.GetStatus()
      if (i == -1):
        rfid = 'TIMEOUT'
      else:
        rfid = ''
        if ((kid & 0x0f) != 0):
          ipg_getrfid = ipguard_pcol.GetRFID()
      #ipg_getrfid = ipguard_pcol.GetRFID()


@app.before_first_request
def start_thread():
  threading.Thread(target=atualizador, args=(), kwargs={}).start()


@app.route('/')
def index():
    return "Hello"



@app.route('/rele', methods=["GET"])
def setRelay():
#-----------------block--------------------
  global block
  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()
    block=True
    
#-----------------block--------------------

    data = request.args.get('data')
    mask = request.args.get('mask')
    res = ''
    if( 'data' in request.args or 'mask' in request.args):
      res = ipguard_pcol.SetOutputs(int(data, base=16), int(mask, base=16))
      
      (i,o,kid) = ipguard_pcol.GetStatus()
#-----------------block--------------------
     
      block=False
#-----------------block--------------------
    if(res == 0):
     
      return jsonify({"message": o}), 200
    else:
      return jsonify({"Res": res}), 200


  
@app.route('/who', methods=["GET"])
def WhoAreYou():
#-----------------block--------------------
  global block
  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()

    block=True
    
#-----------------block--------------------
    (m, v, sn) = ipguard_pcol.WhoAreYou()
#-----------------block--------------------
  
    block=False
#-----------------block--------------------
    return jsonify({"Model": m},{"Version":v},{"SN":sn}), 200

@app.route('/rfid', methods=["GET"])
def rfid():
  #block=True
  #ipg_getrfid = ipguard_pcol.GetRFID()
  #block=False
  return jsonify({"RFID": ipg_getrfid}), 200

@app.route('/rfid/null', methods=["GET"])
def rfidnull():
  global ipg_getrfid
  ipg_getrfid=''
  return '',200

@app.route('/senha', methods=["GET"])
def senha():
  os.system("sudo python3 /var/www/html/scripts/INON.py "+str(rele)+" "+str(delay))
  return 'ok',200


@app.route('/vin', methods=["GET"])
def vin():

#-----------------block--------------------
  global block
  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()

    block=True
   
#-----------------block--------------------  
    o = jsonify({"VIN": str(ipguard_pcol.GetVin())}), 200
#-----------------block--------------------
   
    block=False
    return o
#-----------------block--------------------

@app.route('/output', methods=["GET"])
def output():
#-----------------block--------------------
  global block
  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()

    block=True
   
#-----------------block--------------------  
    (i,o,kid) = ipguard_pcol.GetStatus()   
  
#-----------------block--------------------
    block=False
#-----------------block--------------------
    return jsonify({"output": str(o)}), 200


@app.route('/input', methods=["GET"])
def input():
#-----------------block--------------------
  global block
  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()
    block=True
   
#-----------------block-------------------- 
    (i,o,kid) = ipguard_pcol.GetStatus()
#-----------------block--------------------
    
    block=False
#-----------------block--------------------   
    return jsonify({"intput": str(i)}), 200

@app.route('/ping', methods=["GET"])
def ping():
#-----------------block--------------------
  global block

  if(isUnBlock):
    threading.Thread(target=preventNeverUnblock, args=(), kwargs={}).start()
    block=True
    

#-----------------block-------------------- 
    ipguard_pcol.Ping()
#-----------------block--------------------
   
    block=False
#-----------------block-------------------- 
    return'', 200
@app.errorhandler(Exception)
def handle_error(e):
  global block
  block=False
  time.sleep(0.05)

@app.route('/favicon.ico') 
def favicon():
  return "Z="

def preventNeverUnblock():
  global block
  time.sleep(0.5)
  block=False

if __name__ == '__name__':
    #app.run()
    
    port = '/dev/serial0'
    if (len(sys.argv) == 2):
        port = sys.argv[1]

app.run(host= '0.0.0.0', port=5001,use_reloader=False)
#threading.Thread(target=atualizador, args=(), kwargs={}).start()

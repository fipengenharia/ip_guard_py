#!/usr/bin/env python
# -*- coding: latin-1 -*-
#import time
from myutils import Log
from myutils import DebugBytes
import time
#
UsePyTTY     = False
#
chrSTX  = '\x3c'
#

if UsePyTTY:
  import pytty
else:
  import serial

#v.7 = Show Tx bytes
Verbose_Tx = 0x80
#v.6 = Show Tx deep
Verbose_TxDeep = 0x40
#v.5 = Show Rx bytes
Verbose_Rx = 0x20
#v.4 = Show Rx deep
Verbose_RxDeep = 0x10

#----------------------------------------------------------------------------
iterbytes = lambda buf: map(ord, buf)

def crc8(data):
  crc = 0x00
  for byte in iterbytes(data):
      for i in range(8):
          mix = (crc ^ byte) & 0x01
          crc >>= 1
          if mix:
              crc ^= 0x8c
          byte >>= 1
  return crc

#----------------------------------------------------------------------------
def init (port, baud = 38400, tout = 0.1):
    global s
    if UsePyTTY:
      s = pytty.TTY(port)
      s.baud          = baud
    else:
      s              = serial.Serial()
      s.port         = port
      s.baudrate     = baud
      s.timeout      = tout
      s.open()

#----------------------------------------------------------------------------
def deinit ():
    s.close()

#----------------------------------------------------------------------------
def setTimeout (to):
    if (to == 0):
      to = 0.1
    s.timeout = to

#----------------------------------------------------------------------------
def TxHostCmd (cmd, data='', v = 0):
# Stx Cmd Sz [Data] CRC
  bf = chrSTX + cmd
  sz = len(data)
  bf += chr(sz)
  bf += data

  crc = crc8(bf[0:])
  bf += chr(crc)

  if (v & Verbose_Tx):
      Log("TX:" +  DebugBytes(bf))

  s.write( bf )
  #s.flush()

#----------------------------------------------------------------------------
def RxHostCmd (v = 0):
  decoded = False
  bf = ''
  sz = 0
  stat = 0
  log = ''

  if (v & Verbose_Rx):
      log = "RX:"
  
  while not decoded:
    ibf = s.read()
    if ibf is None:
      ibf = ''
    if not UsePyTTY:
      if (len(ibf) == 0):
        break

    for b in ibf:
      if (v & Verbose_Rx):
        log = log + "%02X" % ord(b) + ' '


      if (stat == 0): # stx
        bf = ''
        stat = 1

      elif (stat == 1): # cmd
        bf = bf + b
        stat = 2

      elif (stat == 2): # sz
        sz = ord(b)
        bf = bf + b
        if (sz == 0):
          stat = 6
        else:
          stat = 4

      elif (stat == 4): # data
        bf = bf + b
        sz = sz - 1
        if (sz == 0):
          stat = 6

      elif (stat == 6): # crc
        rxcrc = ord(b)
        crc = crc8(chrSTX + bf)
        if (rxcrc == crc):
          decoded = True
          break

  if (decoded):
    if (v & Verbose_RxDeep):
        log = log + "Answer Ok"
        Log(log)
  elif (v & Verbose_Rx):
    Log(log)

  return bf

#----------------------------------------------------------------------------
def TxRxHostCmd (cmd, data='', retry = 3, v = 0):
    bf = ''
    if (v & Verbose_TxDeep):
        Log("Tx cmd:" + cmd)
    while (retry):
      ini = time.time()
      TxHostCmd (cmd, data, v)
  #    print "Tx: %f" %(1000*(time.time()-ini))
      ini = time.time()
      bf = RxHostCmd (v)
  #    print "Rx: %f" % (1000*(time.time()-ini))
      if (bf): break
      if (v & Verbose_RxDeep):
         Log("RETRY")
      retry = retry - 1
    return bf

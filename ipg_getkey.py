#!/usr/bin/env python
# -*- coding: latin-1 -*-
import ipguard_pcol
from myutils import Log
import sys

#----------------------------------------------------------------------------
if __name__=='__main__':
    port = '/dev/serial0'
    if (len(sys.argv) == 2):
        port = sys.argv[1]

    ipguard_pcol.init(port)
    (i,o,kid) = ipguard_pcol.GetStatus()
    if (i == -1):
        k = 'TIMEOUT'
    else:
        k = ''
        if ((kid >> 4) != 0):
            k = ipguard_pcol.GetKey()
    print "Key;%s" % (k)
    ipguard_pcol.deinit()

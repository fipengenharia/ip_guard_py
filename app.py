import wx
import threading
import time

exitThread = False
mainPanel  = 0

TEST_SERIAL      = 0
TEST_READ_ADC    = 1
TEST_SET_OUTPUTS = 2
TEST_READ_INPUTS = 3
TEST_ERROR       = 255

#----------------------------------------------------------------------------
# Frame
#----------------------------------------------------------------------------
class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition, size=(400,250), style=wx.DEFAULT_FRAME_STYLE, name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title, pos, size, style, name)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.panel = ExamplePanel(self)
        
        global mainPanel
        mainPanel = self.panel

    def OnClose(self,evt):
      global exitThread
      print "CLOSE!!!"
      exitThread = True
      self.Destroy()  

#----------------------------------------------------------------------------
# Visual
#----------------------------------------------------------------------------
class ExamplePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        
        #Labels
        font = wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.NORMAL)
        self.txtSerial = wx.StaticText(self, label=tests[0][0], pos=(20, 10))
        self.txtSerial.SetFont(font)

        self.stsMessage = wx.StaticText(self, label=tests[0][0], pos=(10, 190))
        self.stsMessage.SetFont(font)

        #daqui pra baixo status
        font = wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.stsSerial = wx.StaticText(self, label="-", pos=(200, 10))
        self.stsSerial.SetFont(font)

        #Botao
        self.button =wx.Button(self, label="Iniciar", pos=(150, 160))
        self.button.SetFont(font)
        self.Bind(wx.EVT_BUTTON, self.onInitTest,self.button)

    def updateValues(self, v, sts):
      if (sts):
        self.stsSerial.SetLabel("OK")
        self.stsSerial.SetForegroundColour((0,255, 0))
      else:
        self.stsSerial.SetLabel("ERR")
        self.stsSerial.SetForegroundColour((255,0,0))

    #status do teste
    def updateStatus(self, sts):
      self.stsMessage.SetLabel(sts)
      print sts


    #status do teste
    def onInitTest(self,event):
      print("Click on object with Id %d" %event.GetId())
      self.quote.SetLabel("oie")

#----------------------------------------------------------------------------
# Thread de leitura dos dados
#----------------------------------------------------------------------------
class myThread (threading.Thread):
   def __init__(self):
      threading.Thread.__init__(self)
      self.counter = 0
      self.test = 0
   def run(self):
      print "Starting " + self.name
      
      while not exitThread:      
        time.sleep(1)
        self.counter +=1

        if (not exitThread):

          #comunicacao serial
          if (self.test == TEST_SERIAL):
            mainPanel.updateStatus("Testando Serial..")
            self.test +=1

          #leitura ADC
          elif (self.test == TEST_READ_ADC):
            mainPanel.updateStatus("Testando ADC..")
            mainPanel.updateValues(self.test, True)
            self.test+=1

          #Setando as saidas
          elif (self.test == TEST_SET_OUTPUTS):
            mainPanel.updateStatus("Testando saidas..")
            mainPanel.updateValues(self.test, False)
            self.test = TEST_ERROR

          #Erro no teste
          else:
            mainPanel.updateStatus("Erro no teste")
            exit()

    

#----------------------------------------------------------------------------
# carraga os testes
#----------------------------------------------------------------------------
tests = [["Comunicacao serial", False],
         ["Leitura ADC"       , False],
         ["Saidas Digitais"   , False],
         ["Entradas digitais" , False],
         ["RFid"              , False],
         ["Teclado"           , False],
         ["Microfone"         , False],
         ["Camera"            , False]]

app = wx.App(False)
#frame = wx.Frame(None)
frame = MyFrame(None, title="The Main Frame")
frame.Show()

#create thread
thread1 = myThread()

# Start new Threads
thread1.start()

app.MainLoop()

